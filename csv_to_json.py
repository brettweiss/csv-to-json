# Command Line script to convert csv file contents to a json document. 
# Each line in csv file becomes json object returned as an array in a json
# document. The output filename (w/o ext.) is used as the json property
# containing the array of json objects. 
#
# The command line arguments are listed below (in order). The arguments
# after output filename will list the json field and type. These 
# arguments need to be in the same order as fields in the csv file. 
#
# Command Line Arguments
    # input file path
    # output file path 
    # file name:type [int, string, boolean, or null]
        # Doesn't support json array or complex for individual field 
#
# Sample: 
# Command Line: python csv_to_json.py input.csv results.json rank:int name:string position:string
# Output: {"results":[{"position": "QB", "name": "Aaron Rodgers", "rank": 1},{"position": "QB", "name": "Drew Brees", "rank": 2}]}
         
import sys
import json

class ObjField:
    def __init__(self, field_name, field_type):
        self.field_name = field_name
        self.field_type = field_type
        
    def field_name(self):
        return self.field_name
        
    def field_type(self):
        return self.field_type
    

def input(args):
    if len(args) < 4:
        raise Exception('Invalid parameter list. Excpected: input file, output file, json field name:[int|string|boolean|null]')
    return {"input_file": args[1].strip(), "output_file": args[2], "fields": handle_obj_fields(args[3:])}

    
def handle_obj_fields(fields):
    obj_fields = []
    for field in fields:
        f = field.split(":")
        obj_fields.append(ObjField(f[0].strip(), f[1].strip()))
    return obj_fields

    
def console_announce(text):
    print '\n----------\n' + text + '\n----------\n'


def create_obj_map(field_array, work_context):
    if (len(field_array) != len(work_context['fields'])):
        raise Exception('The number of the fields in the line do not match number of fields excpected.')
    field_mapping = {}
    for i, field in enumerate(field_array):
        field = work_context['fields'][i]
        if field.field_type == 'string':
            field_mapping[field.field_name] = field_array[i].strip()
        elif field.field_type == 'int':
            field_mapping[field.field_name] = float(field_array[i].strip())
        elif field.field_type == 'boolean':
            field_mapping[field.field_name] = bool(field_array[i].strip())
        elif field.field_type == 'null':
            field_mapping[field.field_name] = None
    return field_mapping


def create_json_document(input_file, output_file):
    json_file = open(output_file, 'w')
    json_file.write('{"' + output_file[(output_file.find('/') + 1):output_file.find('.')] + '":[')
    
    i = 0
    with open(input_file, 'r') as infile:
        for line in infile:
            json_file.write((',' if i != 0 else '') + json.dumps(create_obj_map(line.split(","), work_context)))
            i = i + 1

    json_file.write(']}')
    json_file.close()


#----------------------------------------------------------
console_announce('START SCRIPT') 

work_context = input(sys.argv)
create_json_document(work_context['input_file'], work_context['output_file'])

console_announce('END SCRIPT')
#----------------------------------------------------------